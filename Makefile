all: default run

default:
	ca65 main.asm
	ld65 main.o -o main.nes -t nes
run:
	nestopia main.nes
