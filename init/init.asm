;-------------------------------------------------------------------------------
; Header
;-------------------------------------------------------------------------------

	org $4020

	db "NES", $1a
	db $02 ; 2 * 16KB PRG ROM
	db $01 ; 1 * 8KB CHR ROM
	db %01000001
	db %00000000
	db 0
	db 0,0,0,0,0,0,0

;-------------------------------------------------------------------------------
; Footer
;-------------------------------------------------------------------------------

	org $FFFA
	
	dw nmihandler   ; $FFFA - Interrupt handler
	dw reset        ; $FFFC - Entry point
	dw irqhandler   ; $FFFE - IRQ handler

;-------------------------------------------------------------------------------
; Initialize/Reset NES
;-------------------------------------------------------------------------------

	org $8000

reset:
	sei
	cld

	; disable sound IRQ
	ldx #$40
	stx $4017

	; Initialize the stack register
	ldx #$ff
	txs

	inx ; #$ff + 1 => #$00

	; zero out the ppu registers
	stx $2000
	stx $2001
	stx $4010

VBlankWait1:
	bit $2002
	bpl VBlankWait1

	txa

ClearMemory:
	sta $0000, X
	sta $0100, X
	sta $0200, X
	sta $0300, X
	sta $0400, X
	sta $0500, X
	sta $0600, X
	sta $0700, X
	inx
	bne ClearMemory

VBlankWait2:
	bit $2002
	bpl VBlankWait2
