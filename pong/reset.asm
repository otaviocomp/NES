Reset:
	SEI ; Disables all interrupts
	CLD ; disable decimal mode
	
	; Disable sound IRQ
	LDX #$40
	STX $4017
	
	; Initialize the stack register
	LDX #$FF
	TXS
	
	INX ; #$FF + 1 => #$00
	
	; Zero out the PPU registers
	STX $2000
	STX $2001
	
	STX $4010

VBlankWait1:
	BIT $2002
	BPL VBlankWait1
	
	TXA

CLEARMEM:
	STA $0000, X ; $0000 => $00FF
	STA $0100, X ; $0100 => $01FF
	STA $0200, X ; $0200 => $02FF
	STA $0300, X ; ...
	STA $0400, X
	STA $0500, X
	STA $0600, X
	LDA #$FF
	STA $0700, X ; $0700 => $07FF
	LDA #$00
	INX
	BNE CLEARMEM    
; wait for vblank

VBlankWait2:
	BIT $2002
	BPL VBlankWait2
	
	LDA #$07
	STA $4014
	NOP
	
	; $3F00 => $3F1F - VRAM used to store palletes
	LDA #$3F
	STA $2006
	LDA #$00
	STA $2006
	
	LDX #$00

LoadPalettes:
	LDA PaletteData, X
	STA $2007 ; $3F00, $3F01, $3F02 => $3F1F
	INX
	CPX #$20
	BNE LoadPalettes    

	LDX #$00

Forever:
	jmp Forever
