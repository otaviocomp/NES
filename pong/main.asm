.segment "HEADER"
.byte "NES"
.byte $1a
.byte $02 ; 2 * 16KB PRG ROM
.byte $01 ; 1 * 8KB CHR ROM
.byte %00000001 ; mapper and mirroring
.byte $00
.byte $00
.byte $00
.byte $00
.byte $00, $00, $00, $00, $00 ; filler bytes
.segment "ZEROPAGE" ; LSB 0 - FF
.segment "STARTUP"

;;;;;;;;;;;;;;;;;;;;
;; INITIALIZATION ;;
;;;;;;;;;;;;;;;;;;;;

Reset:
	SEI ; Disables all interrupts
	CLD ; disable decimal mode
	
	; Disable sound IRQ
	LDX #$40
	STX $4017
	
	; Initialize the stack register
	LDX #$FF
	TXS
	
	INX ; #$FF + 1 => #$00
	
	; Zero out the PPU registers
	STX $2000
	STX $2001
	
	STX $4010

VBlankWait1:
	BIT $2002
	BPL VBlankWait1
	
	TXA

CLEARMEM:
	STA $0000, X ; $0000 => $00FF
	STA $0100, X ; $0100 => $01FF
	STA $0200, X ; $0200 => $02FF
	STA $0300, X ; ...
	STA $0400, X
	STA $0500, X
	STA $0600, X
	LDA #$FF
	STA $0700, X ; $0700 => $07FF
	LDA #$00
	INX
	BNE CLEARMEM    
; wait for vblank

VBlankWait2:
	BIT $2002
	BPL VBlankWait2
	
	LDA #$07
	STA $4014
	NOP
	
	; $3F00 => $3F1F - VRAM used to store palletes
	LDA #$3F
	STA $2006
	LDA #$00
	STA $2006
	
	LDX #$00

;;;;;;;;;;;;
;; LABELS ;;
;;;;;;;;;;;;

    input = $4016
    x_pos = $00
    y_pos = $01

;;;;;;;;;;;;;;;;;;;;;;;
;; INITIALIZE LABELS ;;
;;;;;;;;;;;;;;;;;;;;;;;

    lda #$20
    sta x_pos
    lda #$20
    sta y_pos

;;;;;;;;;;
;; MAIN ;;
;;;;;;;;;;

	CLI

LoadPalettes:
	LDA PaletteData, X
	STA $2007 ; $3F00, $3F01, $3F02 => $3F1F
	INX
	CPX #$20
	BNE LoadPalettes    

	LDX #$00

Loop:
	BIT $2002
	BPL Loop

	lda #$07
	sta $2003
	sta $2003

LoadSprites:
	lda SpriteData, x
	sta $0700, x
	inx
	cpx #$20
	bne LoadSprites

	; load sprint
	lda y_pos
	sta $2004
	lda #$07
	sta $2004
	sta $2004
	lda x_pos
	sta $2004

	; read input values
	lda #$01
	sta input
	lda #$00
	sta input

	lda input ; A button
	lda input ; B button
	lda input ; select button
	lda input ; start button

	lda input ; up button
	and #1
	bne UpKey
	lda input ; down button 
	and #1
	bne DownKey
	lda input ; left button
	and #1
	bne LeftKey
	lda input ; right button
	and #1
	bne RightKey
	
	LDA #%10010000
	STA $2000
	
	LDA #%10011110
	STA $2001
	jmp Done

UpKey:
	lda y_pos
	sbc #$1
	sta y_pos
	jmp Done

DownKey:
	lda y_pos
	adc #$1
	sta y_pos
	jmp Done

LeftKey:
	lda x_pos
	sbc #$1
	sta x_pos
	jmp Done

RightKey:
	lda x_pos
	adc #$1
	sta x_pos

Done:
	JMP Loop

NMI:
	LDA #$07 ; copy sprite data from $0700 => PPU memory for display
	STA $4014
	RTI

PaletteData:
  .byte $22,$29,$1A,$0F,$22,$36,$17,$0f,$22,$30,$21,$0f,$22,$27,$17,$0F  ;background palette data
  .byte $22,$16,$27,$18,$22,$1A,$30,$27,$22,$16,$30,$27,$22,$0F,$36,$17  ;sprite palette data

SpriteData:
  .byte $10, $00, $00, $08
  ;.byte $10, $01, $00, $10
  ;.byte $18, $02, $00, $08
  ;.byte $18, $03, $00, $10
  ;.byte $20, $04, $00, $08
  ;.byte $20, $05, $00, $10
  ;.byte $28, $06, $00, $08
  ;.byte $28, $07, $00, $10

.segment "VECTORS"
    .word NMI
    .word Reset

.segment "CHARS"
    .incbin "hellomario.chr"
