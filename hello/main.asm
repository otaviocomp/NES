.include "header.asm"
.include "footer.asm"

.segment "STARTUP"
.segment "CODE"

.include "macros.asm"
.include "reset.asm"

Start:
LoadPalettes:
	lda ppustatus
	lda #$3f
	sta ppuaddr
	lda #$00
	sta ppuaddr
	ldx #$00
@loop:
	lda palettes, x
	sta ppudata
	inx
	cpx #$20
	bne @loop

LoadSprites:
	lda hello, y
	sta $0200, y
	iny 
	cpy #$34
	bne LoadSprites

EnableRendering:
	lda #%10000000	; Enable NMI
	sta ppuctrl
	lda #%00010000	; Enable Sprites
	sta ppumask

Main:
forever:
	jmp forever

nmi:
	lda #$02
	sta oamdma
	rti

hello:
	;Y_Pos, Tile, Attr, X_Pos
	.byte $00, $00, $00, $00
	.byte $00, $00, $00, $00
	.byte $6c, $02, $00, $50
	.byte $6c, $01, $00, $5a
	.byte $6c, $03, $00, $64
	.byte $6c, $03, $00, $6e
	.byte $6c, $05, $00, $78
	.byte $78, $06, $00, $8d
	.byte $78, $05, $00, $97
	.byte $78, $04, $00, $a1
	.byte $78, $03, $00, $ab
	.byte $78, $00, $00, $b5

palettes:
	; Background Palette
	.byte $0f, $00, $00, $00
	.byte $0f, $00, $00, $00
	.byte $0f, $00, $00, $00
	.byte $0f, $00, $00, $00

	; Sprite Palette
	.byte $0f, $1a, $00, $00
	.byte $0f, $00, $00, $00
	.byte $0f, $00, $00, $00
	.byte $0f, $00, $00, $00

.include "chars.asm"
