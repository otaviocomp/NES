.include "header.asm"
.include "footer.asm"
.include "macros.asm"
.include "reset.asm"

MainLoop:
	lda #$01
	sta $4015
	lda #$08
	sta $4002
	lda #$02
	sta $4003
	lda #$01
	sta $4000
	jmp MainLoop

NMI:
	rti

IRQ:
	rti
