.segment "CODE"

.macro WAIT_VBLANK
:	bit ppustatus
	bpl :-
.endmacro

Reset:
	sei ; Disables all interrupts
	cld ; disable decimal mode
	ldx #$40
	stx $4017
	ldx #$FF
	txs
	inx ; #$FF + 1 => #$00
	stx ppuctrl
	stx ppumask
	stx $4010
	txa

	WAIT_VBLANK

ClearMemory:
	sta $0000, X ; $0000 => $00FF
	sta $0100, X ; $0100 => $01FF
	sta $0200, X ; $0200 => $02FF
	sta $0300, X
	sta $0400, X
	sta $0500, X
	sta $0600, X
	sta $0700, X
	inx
	bne ClearMemory

	WAIT_VBLANK
