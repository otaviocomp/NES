.segment "STARTUP"

.include "reset.asm"

;;;;;;;;;;;;
;; LABELS ;;
;;;;;;;;;;;;

    input = $4016
    x_pos = $00
    y_pos = $01

;;;;;;;;;;;;;;;;;;;;;;;
;; INITIALIZE LABELS ;;
;;;;;;;;;;;;;;;;;;;;;;;

    lda #$10
    sta y_pos
    lda #$08
    sta x_pos

Loop:
	BIT $2002
	BPL Loop

	lda #$00
	sta $2003
	sta $2003

	; load sprint
	lda y_pos
	sta $2004
	lda #$00
	sta $2004
	sta $2004
	lda x_pos
	sta $2004

	; read input values
	lda #$01
	sta input
	lda #$00
	sta input

	lda input ; A button
	lda input ; B button
	lda input ; select button
	lda input ; start button

	lda input ; up button
	and #1
	bne UpKey
	lda input ; down button 
	and #1
	bne DownKey
	lda input ; left button
	and #1
	bne LeftKey
	lda input ; right button
	and #1
	bne RightKey
	
	jmp Done

UpKey:
	lda y_pos
	sbc #$1
	sta y_pos
	jmp Done

DownKey:
	lda y_pos
	adc #$1
	sta y_pos
	jmp Done

LeftKey:
	lda x_pos
	sbc #$1
	sta x_pos
	jmp Done

RightKey:
	lda x_pos
	adc #$1
	sta x_pos

Done:
	JMP Loop

NMI:
	LDA #$02 ; copy sprite data from $0200 => PPU memory for display
	STA $4014
	RTI

PaletteData:
  .byte $22,$29,$1A,$0F,$22,$36,$17,$0f,$22,$30,$21,$0f,$22,$27,$17,$0F  ;background palette data
  .byte $22,$16,$27,$18,$22,$1A,$30,$27,$22,$16,$30,$27,$22,$0F,$36,$17  ;sprite palette data

SpriteData:
  .byte $10, $00, $00, $08
  .byte $10, $01, $00, $10
  .byte $18, $02, $00, $08
  .byte $18, $03, $00, $10
  .byte $20, $04, $00, $08
  .byte $20, $05, $00, $10
  .byte $28, $06, $00, $08
  .byte $28, $07, $00, $10

.segment "VECTORS"
    .word NMI
    .word Reset

.segment "CHARS"
    .incbin "hellomario.chr"
